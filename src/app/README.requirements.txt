# How to Create a Python Requirements File


# criar o requirents.txt todas os modulos que eu possuo
pip freeze > requirements.txt


# Adding Modules to Your Python Requirements File



# Installing Python Packages From a Requirements File
pip install -r requirements.txt


# How to Maintain a Python Requirements Fil
# Step 1: Output a list of outdated packages with
pip list --outdated.

# Step 2: Upgrade the required package
pip install -U PackageName
pip install -U fastapi

pip install -U -r requirements.txt.


# How to Create Python Requirements File After Development
pip install pipreqs
pipreqs .
pipreqs /home/project/location

# Install all packges fro  requirements
pip install -r requirements.txt


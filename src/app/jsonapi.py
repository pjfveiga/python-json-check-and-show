from flask import Flask, jsonify, request, json

app = Flask(__name__)

incomes = [
    { 'description': 'salary', 'amount': 5000 }
]


@app.route('/incomes')
def get_incomes():
     return json.dumps(incomes)


@app.route('/incomes', methods=['POST'])
def add_income():
     data = request.get_json()
     incomes.append(data)
     resp = jsonify({"status": 200,'body': "pred_str","headers" : {
                        "Content-Type" : "application/json"
                        }
                        })
     resp = jsonify(incomes)
#     return resp,204
     return json.dumps(data), 204
#     return json.dumps(data), 204
# #    return jsonify(request.get_json()), 204

if __name__ == "__main__":
    app.run(port=5001)
